$(document).ready(function(){
    $('.wrap a').bind('click',function(event){
        var $anchor = $(this);

        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top - 200
        }, 1600,'easeOutExpo');

        event.preventDefault();
    });

    $('.flexslider').flexslider({
        animation: "fade",
        animationLoop: true,
        slideshowSpeed: 10000,
        animationSpeed: 1000,
        initDelay: 0,
        itemWidth: 1000,
        itemMargin: 0
    });

    $('#tabpresentacion a').click(function (e) {
      e.preventDefault();
      $(this).tab('show');
    });

    $('#tabareas a').click(function (e) {
      e.preventDefault();
      $(this).tab('show');
      console.log($(this));
    });

    function validateEmail(sEmail) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
         return true;
        }
        else {
            return false;
        }
    }
  $("#submit").click(function() {
        // validate and process form
        // first hide any error messages
        $('.error').removeClass('error');
        $('#erroricon').fadeOut();
        $('#errortext').fadeOut();
        var error_ = false
        var nombre = $("input#nombre").val();
        if (nombre == "" || nombre == "Nombre Completo") {
          $("input#nombre").addClass('error');
          error_ = true;
        }
        var email__ = $("input#email").val();
        if (email__ == "" || !validateEmail(email__)) {
          $("input#email").addClass('error');
          error_ = true;
        }
        var asunto = $("input#asunto").val();
        if (asunto == "" || asunto == "Asunto") {
          $("input#asunto").addClass('error');
          error_ = true;
        }
        var mensaje = $("#mensaje").val();
        if (mensaje == "" || mensaje == "Mensaje") {
          $("#mensaje").addClass('error');
          error_ = true;
        }
        if (error_){
          $('#errortext').fadeIn();
          return false;
        }
        var dataString = 'email-confirmation=' + '1' + '&nombre=' + nombre + '&email=' + email__ + '&asunto=' + asunto + '&mensaje=' + mensaje;
        $.ajax({
            type: "POST",
            url: "process.php",
            data: dataString,
            success: function() {
              $('#formform').fadeOut();
              $('#gracias').fadeIn(2000);
            }
          });
          return false;
    });

});

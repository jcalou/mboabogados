var gulp = require('gulp'),
    sass = require('gulp-sass'),
    concatCss = require('gulp-concat-css'),
    minifyCSS = require('gulp-minify-css'),
    del = require('del'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify');

gulp.task('css', function () {
  return (
    gulp.src('./assets/sass/**/*.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(concatCss('all.css'))
    .pipe(minifyCSS())
    .pipe(gulp.dest('./assets/build/'))
  )
});

gulp.task('vendorscripts', function() {
  return (
    gulp.src('./assets/js/vendor/**/*.js')
    .pipe(concat('vendor.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./assets/build/'))
  )
});

gulp.task('scripts', function() {
  return (
    gulp.src('./assets/js/all/**/*.js')
    .pipe(concat('all.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./assets/build/'))
  )
});

// gulp clean - cleans the dist folder
gulp.task('clean', function () {
  return del([
    './assets/build/**/*'
  ]);
});

gulp.task('watch', ['build'], function() {
  gulp.watch('./assets/sass/**/*.scss', ['css']);
  gulp.watch('./assets/js/vendor/**/*.js', ['vendorscripts']);
  gulp.watch('./assets/js/all/**/*.js', ['scripts']);
});

gulp.task('build', ['css', 'vendorscripts', 'scripts']);

gulp.task('default', ['watch']);
